#### This project is for the Devops Bootcamp Exercise for 
#### "Kubernetes on AWS - EKS" 

#### Demo Project:

Complete CI/CD Pipeline with EKS and AWS ECR

#### Technologiesused:

Kubernetes, Jenkins, AWS EKS, AWS ECR, Java, Gradle, Linux, Docker, Gitlab

#### Project Description

#### A. Deployed Java-Mysql Application.
- Created a private AWS ECR Docker repository.
- Created EKS cluster via "eksctl command" with 3 Nodes and 1 Fargate profile.
- Deployed Mysql (via helm) and Phpmyadmin.
- Deployed Java-Mysql Application.

#### B. Automated the Deployment of my Java-Mysql App using CI/CD.

- Setup CI/CD Pipline with Jenkins to build and push Docker Image to AWS ECR
- Integrated deploying to K8s cluster in the CI/CD pipeline from AWS ECR private registry
- The complete CI/CD project we built has the following configuration:

    a. CI step: Build artifact for Java Maven application

    b. CI step: Build and push Docker image to AWS ECR

    c. CD step: Deploy new application version to EKS cluster

    d. CD step: Commit the version update

- Configued Autoscaling to scale dpnw to 1 node when server are underutilized and 3 Nodes when at full capacity.
